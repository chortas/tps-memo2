class SourceValidator
  def process(gemfile_content)
    gemfile_content.include? 'source'
  end
end
