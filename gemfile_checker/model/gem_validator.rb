class GemValidator
  def process(gemfile_content)
    lines = gemfile_content.split("\n")
    lines.each do |line|
      lines.delete(line) unless line.include? 'gem '
    end
    lines == lines.sort
  end
end
