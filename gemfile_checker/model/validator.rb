require_relative 'gem_validator'
require_relative 'ruby_validator'
require_relative 'source_validator'

class Validator
  def process(gemfile_content)
    !gemfile_content.empty? && GemValidator.new.process(gemfile_content) && \
      RubyValidator.new.process(gemfile_content) && SourceValidator.new.process(gemfile_content)
  end
end
