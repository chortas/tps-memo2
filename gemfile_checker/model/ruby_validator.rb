class RubyValidator
  def process(gemfile_content)
    gemfile_content.each_line do |line|
      return true if line.match(/ruby '[0-9].[0-9].[0-9]'/)
    end
    false
  end
end
