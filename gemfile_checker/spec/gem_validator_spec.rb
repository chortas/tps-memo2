require 'rspec'
require_relative '../model/gem_validator'

describe GemValidator do
  subject { @gem_validator = described_class.new }

  it { is_expected.to respond_to :process }

  it 'should return false if gems are not ordered alphabetically' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.gems"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end
end
