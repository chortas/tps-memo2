require 'rspec'
require_relative '../model/ruby_validator'

describe RubyValidator do
  subject { @ruby_validator = described_class.new }

  it { is_expected.to respond_to :process }

  it 'should return false if gemfile does not have a ruby version' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.ruby"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end
end
