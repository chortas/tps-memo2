require 'rspec'
require_relative '../model/source_validator'

describe SourceValidator do
  subject { @validator = described_class.new }

  it { is_expected.to respond_to :process }

  it 'should return false if gemfile does not begin with source line' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.source"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end
end
