require 'rspec'
require_relative '../model/validator'

describe Validator do
  subject { @validator = described_class.new }

  it { is_expected.to respond_to :process }

  it 'should return false if gemfile is empty' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.empty"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false if gemfile does not begin with source line' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.source"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false if gemfile does not have a ruby version' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.ruby"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false if gems are not ordered alphabetically' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.gems"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return true when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq true
  end
end
