# parser => file_reader
# validator => model
# presenter => print
require_relative './model/validator'
require_relative './model/gem_validator'
require_relative './model/ruby_validator'
require_relative './model/source_validator'

gemfile_content = File.read ARGV[0]

validation_result = Validator.new.process gemfile_content

if validation_result
  puts 'Gemfile correcto'
elsif gemfile_content.empty?
  puts 'Error: Gemfile vacio'
elsif !GemValidator.new.process gemfile_content
  puts 'Error: Gemfile con gemas en desorden'
elsif !RubyValidator.new.process gemfile_content
  puts 'Error: Gemfile sin ruby version'
elsif !SourceValidator.new.process gemfile_content
  puts 'Error: Gemfile sin source'
end
